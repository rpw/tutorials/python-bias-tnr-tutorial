#!/bin/bash

# script to get data required to run the tutorial
# Data files are requested using the SOAR TAP API (see http://soar.esac.esa.int/soar/#home)
# This script requires wget tool to work.

# Author: Xavier Bonnin (LESIA, CNRS)
# Date: 14/09/2022


# Define the path of the directory where downloaded file will be saved
DATA_DIR="${PWD}/data"
# Create the directory where data will be downloaded if it doesn't exist
mkdir -p "${DATA_DIR}"

# Build SOAR API
API_URL_TEMPLATE="http://soar.esac.esa.int/soar-sl-tap/data?retrieval_type=LAST_PRODUCT&product_type=SCIENCE&data_item_id="

# Loop over the data items we want to download
for DATA_ITEM in solo_L2_rpw-tnr-surv_20201113 solo_L2_rpw-tnr-surv_20201118 solo_L2_rpw-tnr-surv_20210512 solo_L3_rpw-bia-density_20201113 solo_L3_rpw-bia-density_20201118 solo_L3_rpw-bia-density_20210512;do
  # Make a web request to receive the data product in this case using wget
  wget -nc --content-disposition -P "${DATA_DIR}" "${API_URL_TEMPLATE}${DATA_ITEM}" --progress=bar:force:noscroll
done

echo "Done"
