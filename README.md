# Python BIAS TNR tutorial

This holds a tutorial to read and plot RPW BIAS and TNR data using the [pyrfu module](https://pypi.org/project/pyrfu/).

## Overview

The tutorial is divided in three items:

1. `get_data.sh` script to download required data files from SOAR. Files are saved in /data folder.
2. `pre-install.sh` script to install Python dependencies
3. `RPW_BIAS-TNR_data_tutorial.ipynb` Jupyter notebook, which contains the tutorial

## Run tutorial on Binder

You can try the tutorial on Binder: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.obspm.fr%2Frpw%2Ftutorials%2Fpython-bias-tnr-tutorial.git/main)

## Run tutorial in a local machine

Before running the tutorial on your local machine, make sure that the following software are installed:
- Python 3, including pip
- wget

First download tutorial files, then run the following commands from the tutorial directory to retrieve data files and install Python dependencies:
```
/bin/bash get_data.sh
/bin/bash pre-install.sh
```

NOTE: It is recommended to install and run the tutorial in a [virtual environment](https://docs.python.org/3/library/venv.html).

Open and run the notebook using a jupyter server. For instance, notebook can be open in a Web browser using the command:

```
jupyter notebook RPW_BIAS-TNR_data_tutorial.ipynb &
```

See [Jupyter documentation](https://jupyter.org/) about how to use notebook.

## Authors

* Louis Richard, IRFU (louisr@irfu.se)

## License

RPW tutorial is released under MIT license.

