#!/bin/bash

# Convert Jupyter notebooks to html files
for current_file in `ls *.ipynb`;do
  jupyter nbconvert --to html $current_file
done

