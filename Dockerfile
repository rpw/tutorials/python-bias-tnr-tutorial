FROM python:3.8
ENV PYTHONUNBUFFERED=1

# Use bash as default shell
ENV SHELL /bin/bash

# create user with a home directory
ARG NB_USER="rpw_user"
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}

RUN adduser --disabled-password \
    --shell /bin/bash \
    --gecos "Default user" \
    --uid ${NB_UID} \
    ${NB_USER}
WORKDIR ${HOME}
# Make sure the contents of our repo are in ${HOME}
COPY . ${HOME}

# Make sure the $HOME folder belongs to user
RUN chown -R ${NB_UID} ${HOME}

# Set $NB_USER as default user
USER ${NB_USER}

# Install dependencies
RUN /bin/bash ${HOME}/pre-install.sh
