#!/bin/bash

# Create and add .local/bin to PATH
mkdir -p ~/.local/bin
export PATH=~/.local/bin":$PATH"

# Install the pyrfu module using pip
python3 -m pip install pyrfu

# Make sure that pyrfu was installed properly
python3 -c "import pyrfu"

# Install jupyter lab and jupyter notebook
python3 -m pip install jupyterlab
python3 -m pip install notebook




